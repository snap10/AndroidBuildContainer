#!/usr/bin/env sh

#fail on error
set -e

# Login to the registry. the variable is automatically set, build and push
TS=$(date +%Y%m%d%H%M)
docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
docker build -t $CI_REGISTRY_IMAGE:$TS .
docker push $CI_REGISTRY_IMAGE:$TS
docker tag $CI_REGISTRY_IMAGE:$TS $CI_REGISTRY_IMAGE:latest
docker push $CI_REGISTRY_IMAGE:latest

exit 0
